---
layout: handbook-page-toc
title: 5 Minute Production App Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## 5 Minute Production App Single-Engineer Group

The 5 Minute Production App is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

The 5 Minute Production App group aims to make it easier for Web App developers to quickly get their application deployed to Production, and to have the re-assurance that it can scale when needed.
