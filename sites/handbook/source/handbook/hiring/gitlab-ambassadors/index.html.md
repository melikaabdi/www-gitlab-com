---
layout: handbook-page-toc
title: "GitLab talent ambassador"
description: "Keeping with our mission that everyone can contribute, we want all GitLab team members to feel encouraged and equipped to take part in helping us find great talent and act as ambassadors for the company and our talent brand."
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Become a GitLab talent ambassador

It's an exciting time to be part of the GitLab team. As we continue to grow, our goal is to create and foster a culture of talent acquisition across the company.

Whether you're a hiring manager or an individual contributor, you play an important role in this.
Keeping with [our mission](/company/mission/#mission) that everyone can contribute, we want all GitLab team members to feel encouraged and equipped to take part in helping us find great talent and act as ambassadors for the company and our talent brand.

Here are a few key things you can do to become a **talent ambassador for GitLab.**

## How to get started


### 1. Request a LinkedIn Recruiter account

If you're a hiring manager or part of a hiring team, you should upgrade your LinkedIn account to a "LinkedIn Recruiter" seat.

*LinkedIn's analytics on GitLab show that our hiring managers have a 43% higher inMail acceptance rate than recruiters or sourcers.*

**With a LinkedIn Recruiter seat, you'll be able to:**
- Collaboratively source candidates with other team members via Projects
- Send unlimited InMails (InMail metrics is one of our KPIs)

**Ready to upgrade your account?**

Learn more about LinkedIn Recruiter and how to [upgrade your account.](/handbook/hiring/sourcing/#upgrading-your-linkedin-account)

**Already upgraded your account?** Awesome! [Check out Step 5](/handbook/hiring/gitlab-ambassadors/#start-sourcing), it's time to start sourcing.

______

### 2. Optimize your profiles

Whether they're just interested in GitLab or have already been sourced or interviewed for a role, most candidates have done their homework on their prospective manager and team.
If you're a hiring manager or part of the hiring team, you'll want your LinkedIn profile (and any other sites you use for work, like Twitter) to represent you and GitLab in the best way.

Here are some steps you can take to get started optimizing your profile.

**1. Name, photo, and headline**
- Use your real name.
- Include a headshot that represents you professionally.
- Write a headline that creatively explains your role or passion.

**2. Use a banner image**
- Your banner image is a great way to visually spread the word about GitLab. Consider using an image from the [marketing repository](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/social-media/profile-assets/png/assets/general).

**3. Write a summary**
- Your summary should mirror your personal elevator pitch. It should tell people who you are and what you're passionate about.
- It's also a great place to talk about your team and GitLab, and why a candidate would want to work with you.
- Feel free to use and build from this boilerplate summary about working at GitLab:

>  It’s an exciting time to be part of the GitLab team. As the largest all-remote company in the world with team members in more than 65 countries, this is a place where you can contribute from almost anywhere. You’ll be part of an ambitious, productive team that embraces a set of shared values in everything we do. As part of GitLab's (YOUR TEAM) team, my role is to (ADD CONTEXT HERE ABOUT YOUR ROLE AND TEAM).

**4. Your current role**
- This is an important one people often miss: Make sure you've listed your current role and connected it to the GitLab page. You'll know this worked when you see the GitLab logo next to your current role.

**5. Follow [GitLab's company page](https://www.linkedin.com/company/gitlab-com/)**

- You'll be able to stay up to date on what our social media team is sharing about GitLab.

**6. Add media**
- Videos about your team or life at GitLab
- Blog posts
- Your [README file](/handbook/marketing/inbound-marketing/digital-experience/website/#creating-and-publishing-your-gitlab-readme)

#### More training for rocking your profile on LinkedIn

LinkedIn's "Rock Your Profile" session for GitLab (held 2020-02-28):
- [Watch the recording](https://gitlab.zoom.us/rec/play/7JQvduygpm03HILHsASDVPF5W9S8KfmshCdN-vcOnxrmBXILNlCnMrcUZbNqFhhr7peLThzLGQANuhue?startTime=1582907384000)
- [View the slides](https://view.pointdrive.linkedin.com/presentations/81ac57f1-75b8-4b3c-95bb-eeef8008aab4?auth=d54802e3-29ec-4755-8cea-5bc1a0e090e0)

______

### 3. Share life at GitLab with the World

Candidates will do their research in places far beyond our jobs site to find out more about whether GitLab is the right fit for them.
We want to be sure we're telling an authentic story about life at GitLab, and one of the best ways to do that is through our team members.
You can help amplify that story by tapping into your networks on social media and sharing what it's like to be part of our team.

Once you've checked out the [social media guidelines](/handbook/marketing/social-media-guidelines/), here are some ideas for types of content you can share to help candidates learn more about GitLab and your team in particular:
- Blog posts from the [Culture section](/blog/categories/culture/) of the GitLab blog
- Videos about life at GitLab from our YouTube channel ([here's an example](https://youtu.be/V2Z1h_2gLNU))
- A blog post [you've written](/handbook/marketing/blog/unfiltered/) about what it's like to work here/on your team
- [Videos about your team or job family](/handbook/hiring/vacancies/#add-a-video) or what it's like to work on your team ([example](https://youtu.be/KH0knfmwgvc)).
- Photos of meetups or coworking days with GitLab team members
- Articles about GitLab in the news - follow the #newswire channel in Slack for a great source of daily updates
- Your [README file](/handbook/marketing/inbound-marketing/digital-experience/website/#creating-and-publishing-your-gitlab-readme)
- Consider hosting a public AMA (Ask Me Anything) on [social media](/handbook/marketing/social-media-guidelines/) or YouTube about working or life at GitLab, using the `#LifeAtGitLab` hashtag. Or, record a [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) with a team member about life at GitLab and upload it to the `Working at GitLab` playlist on [GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0KqNM5pnenLqkA9pIoFO5edr).

Here are some [talent brand resources](/handbook/people-group/employment-branding/#talent-brand-resources) you can reference for our latest team headcount, awards, blogs, and more facts about working at GitLab.

#### On LinkedIn

- Don't be afraid to share more than just articles on LinkedIn. Post when you're attending an event with GitLab, share a photo of where you're working today, or consider writing a blog post about working at GitLab.
- As you're sharing this content on LinkedIn, use the hashtags `#LifeAtGitLab` and `#AllRemote` as you deem suitable. This will connect your post to a curated feed of content from our team members on [GitLab's "life" tab](https://www.linkedin.com/company/gitlab-com/life) on LinkedIn, and promote our 100% remote culture.

#### On Twitter

- Be sure to follow the [@gitlab Twitter account](https://twitter.com/gitlab) and reshare content our [social media team](/handbook/marketing/corporate-marketing/social-marketing/) is posting.
- Consider tweeting some of the content ideas above, and tag the GitLab account for potential retweets.
- Just like on LinkedIn, use the hashtags `#LifeAtGitLab` and `#AllRemote` as you deem suitable in your tweets.
- If you're sharing a job opening, here are a few suggestions you can consider sharing with the job link. Feel free to personalize these:

>  It’s an exciting time to join @gitlab. We're hiring for a [INSERT ROLE NAME]! [INSERT JOB LINK] #AllRemote #LifeAtGitLab

>  Interested in joining the world's largest #AllRemote company? @gitlab is hiring a [INSERT ROLE NAME]! [INSERT JOB LINK] #LifeAtGitLab

>  Do your best work from anywhere. @gitlab is the world's largest #AllRemote company, and we're hiring a [INSERT ROLE NAME]. Interested? Learn more: [INSERT JOB LINK] #LifeAtGitLab


#### Other platforms

There are a number of platforms and touchpoints where candiates interact with our talent brand or our jobs prior to ever applying or talking with us.
Here are a couple of ways you can help tell GitLab's story on these sites.

- Leave a [Glassdoor review](https://www.glassdoor.com/mz-survey/start_input.htm?cr=&c=&showSurvey=Reviews)
- Rate GitLab [on Comparably](https://www.comparably.com/companies/gitlab/survey?s=hc5x4)

Know of another site where GitLab should have a talent acquisition or talent brand profile? Send us a message in the `#talent-brand` channel on Slack.

-----

### 4. Refer great people to GitLab

One of the best ways to help GitLab grow is by referring talented people in your network.

As you think about who would be a good fit for GitLab, don't limit yourself to only the people you know are actively searching for a job. You never know when someone is ready for their next career journey.
We want to hire the best candidate for the role and make sure that person will be a ["values add"](/handbook/values/#culture-fit-is-a-bad-excuse) to GitLab's unique [culture](/company/culture/).

Here's a [fact sheet](https://gitlab.com/gitlab-com/people-group/talent acquisition/-/blob/master/Hiring_fact_sheet_-_2020-03.pdf) that can be used as a reference guide as you're having conversations with potential candidates.

**Ready to make your first referral?** Check out [this guide.](/handbook/hiring/referral-process/)

-----

### 5. Start sourcing

Once you've activated your LinkedIn Recruiter licensce and your public profiles are updated, you're ready to start sourcing candidates for your team.

You can use this [sourcing guide](/handbook/hiring/sourcing/#how-to-source-candidates) to learn how we define a sourced candidate, where to start your search, and even what you could consider saying in your outreach.

For additional guidance on using LinkedIn Recruiter for sourcing, check out this [training page](https://business.linkedin.com/talent-solutions/cx/18/05/fast-track#all).

**Keep in mind:**
- As a hiring manager, you can request a [source-a-thon](/handbook/hiring/sourcing/#source-a-thons) for your vacancy. The Talent Acquisition team will partner with you to schedule it, and anyone is welcome to participate!
- There's a difference between a sourced candidate and a referral. [Here's a quick overview](/handbook/hiring/referral-process/#defining-a-referral).
- If you have questions, you can always reach out to your [talent acquisition partner](/handbook/hiring/talent-acquisition-alignment/) or post it in the `#talent-acquisition` channel on Slack.

#### Responding to potential candidates

As you share more about life at GitLab and get involved in sourcing candidates, you'll likely start to get more direct outreach from people interested in working here.

Here's a suggestion for how to respond to these messages, particularly if you don't know the potential candidate.
Feel free to make this your own and personalize it based on the specific message or questions you received:

>  Hi, {NAME}, thank you for your interest in GitLab!
It's an exciting time to be part of our team. We're the largest all-remote company in the world, with team members in more than 65 countries.
The best way to share your information with our talent acquisition team is to [join our talent community](https://boards.greenhouse.io/gitlab/jobs/4700367002), but first, I encourage you to take a look through our [company handbook](/handbook/) to learn a bit more about our culture.
You can find out more about our hiring process on the [Jobs FAQ page](/jobs/faq/#gitlabs-outbound-talent-acquisition-model), and our [all-remote page](/company/culture/all-remote/) is an awesome resource to explore GitLab's approach to remote work.
I also recommend reading through our [company values](/handbook/values/) to understand how we work and interact as a team.
GitLab is an incredibly unique place to work, and I'm happy to point you in the right direction if you have more questions.

Thanks again for your interest in joining the team.
All the best,
{YOUR NAME}



